# Pr�ctica 7: Uso de gdb para depurar un programa con listas enlazadas #

Este es un repositorio que contiene un programa que lee datos de temperatura de un archivo tabulado. El programa almacena los datos leidos en una lista enlazada. Al ejecutar, el programa produce una falla de segmentaci�n. Utilice gdb para depurar y corregir el error.

### Uso ###
El programa usa argumentos para determinar el nombre del archivo a leer. Para leer un archivo tabulado con nombre *datos.txt*:

```
./lector datos.txt
```

Si la primera l�nea del archivo de datos contiene una cabecera, le podemos pedir al programa que la ignore usando la opci�n -c:

```
./lector -c datos.txt
```

### �C�mo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en su cuenta (no este) en su computadora del laboratorio
* Completar la pr�ctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###

Edite esta lista y a�ada los nombres de los integrantes, luego borre esta l�nea.

* Integrante 1
* Integrante 2